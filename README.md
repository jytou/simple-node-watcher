# simple-node-watcher
Un outil simple en shell pour surveiller des nœuds et d'éventuels forks. C'est pas optimisé, c'est du shell bash hardcore, ça se base uniquement sur du http, c'est codé à l'arrache, faut remplir un fichier à la main, mais ça marche très bien et comme les nœuds sont interrogés en parallèle, ça va très vite. Non, ça ne fait pas le café.

Pour commencer, téléchargez le script du projet.
 
Ensuite, créez dans le même répertoire que le script un fichier ```nodes.txt``` comme celui qui se trouve dans ce projet.

La syntaxe est simplissime : un nœud par ligne, mettez un # en début de ligne pour ignorer temporairement un nœud, et un ! en fin de ligne (**séparé du nom par un espace**) pour votre nœud : il apparaîtra en gras et vous saurez d'un coup d'œil s'il est sur une bonne branche ou s'il est parti en fork.

Enfin, lancez le script : ./checknodes.sh

Exemple de résultat (oui, je sais j'ai des goûts de ch***e, et alors ?) :

![Fork|237x500](result_fork.jpg) 

Autre exemple en s'en tenant aux nœud qui sont sur la branche principale :

![Ok|277x348](result_ok.jpg) 

On peut voir:
- les différentes branches avec leur numéro de bloc + le début de son hash et les nœuds sur ces blocs, tout ça bien sûr trié par ordre alphabétique,
- les nœuds qui sont en http,
- les nœuds en erreur à la fin,
- un compte rendu sur l'état de la blockchain avec le nombre de forks pour les nœuds surveillés (rien ne dit que d'autres nœuds ne viennent pas mettre la pagaille ailleurs),
- le compteur en bas représente le temps qui reste avant le prochain scan.

Vous pouvez aussi modifier en début de fichier 3 paramètres :

- le temps d'attente entre 2 scans (en secondes),
- les timeout pour les nœuds en https et http.

À utiliser sans modération, n'hésitez pas à faire des modifs/améliorations - à copier, redistribuer, modifier, exécuter, à vos risques et périls. Ne venez pas vous plaindre à moi si vous êtes englouti par une avalanche ! :D 

Exemples d'améliorations ou customisations :

- envoyer un mail en cas de fork (et en ne conservant dans la liste que les nœuds les plus « sains » pour ne pas recevoir un mail toutes les 5 minutes),
- pour chaque nœud, faire également un scan sur /network/peers, récupérer la liste des peers et boucler sur les peers qu'on n'a pas encore scannés, en boucle jusqu'à avoir tout scanné, ça serait assez facile à faire mais en quelques heures, mais ça serait du coup un excellent observateur du réseau tout entier à peu de frais…
- avec [ça](https://github.com/hashrocket/ws) observer également les nœuds en WebSocket…

Oui, je sais, j'ai trop d'idées et pas assez de temps pour les implémenter, comme d'hab…