#!/bin/bash

# Sleep time in seconds
sleep=300

# Checks the current block number of a node - is run in parallel and stores the result in the outfile
# Parameters: <node> <all watched nodes> <outfile>
checkonenode()
{
	# Timeout in seconds for https nodes
	httpsTimeout=10
	# Timeout in seconds for http nodes
	httpTimeout=3

	node=$1
	watched=$2
	outfile=$3
	# Curl: -m timeout, -k ignore SSL certificate errors
	cur=`echo "$( { curl -m $httpsTimeout -k https://$node/blockchain/current; } 2>&1 )"`
	n=$node
	if [[ "$cur" != *issuersFrameVar* ]]
	then
		# It failed in https, maybe try http?
		cur=`echo "$( { curl -m $httpTimeout http://$node/blockchain/current; } 2>&1 )"`
		if [[ "$cur" == *issuersFrameVar* ]]
		then
			# Indicate that the node is http
			n="$n-(http)"
		fi
	fi
	if [[ "$cur" != *issuersFrameVar* ]]
	then
		# The node didn't respond on time
		cur="ERROR"
	else
		# The node did respond - grab the block number and hash of the block as key
		cur="`echo "$cur"|grep '^  "number": '|awk '{print $2}'|awk -F, '{print $1}'`-`echo "$cur"|grep '^  "hash": '|awk '{print $2}'|awk '{print substr($1,2,13)}'`"
	fi
	if [[ $watched =~ .*#$node#.* ]]
	then
		# The node is a watched node, add some bold
		n="\e[1m$n\e[0m"
	fi
	# Put the result into the file
	echo "$cur $n">$outfile
	# Notify that we're done here
	touch $outfile.done
}

# Temp dir where results are stored
DIR=/tmp/gnodewatch
export DIR
mkdir -p $DIR

# Main loop
while [ true ]
do
	# Grab the nodes we are actively wotching - they will be in bold in the final output
	watched=`grep -v "#" nodes.txt|egrep "\!$"|awk '{print "#" $1 "#"}'`
	# All nodes we are watching
	nodes=`grep -v "#" nodes.txt|awk '{print $1}'`
	# The index to generate separate file names
	index=0
	# Wipe out the output directory
	rm $DIR/*out $DIR/*done 2>/dev/null

	printf "Probing nodes..."
	# Query all nodes in parallel
	for node in $nodes
	do
		checkonenode $node "$watched" $DIR/$index.out &
		((index++))
	done

	# Wait a little for the first files to be created
	sleep 1s
	# Wait for all the threads to report they are done
	while [ `ls $DIR/*done|wc -l` -lt $index ]
	do
		sleep 1s
	done

	printf "\r                       \r"
	# Grab all results
	curs=`cat $DIR/*out|sort`
	# Extract all forks, excluding all errors
	chains="`echo "$curs"|grep -v ERROR|awk '{print $1}'|sort -r|uniq`"

	# Count the number of chains and output the corresponding nodes
	nb=0
	for chain in $chains
	do
		echo $chain
		echo -e "`echo "$curs"|egrep "^$chain "|awk '{print "|      " $2}'`"
		((nb++))
	done

	# Output the nodes in error
	if [[ "`echo "$curs"|grep ERROR`" != "" ]]
	then
		echo ""
		echo "Nodes in error:"
		echo -e "`echo "$curs"|grep ERROR|awk '{print "|      " $2}'`"
	fi
	echo ""

	# Output a final status
	if [[ $nb == 0 ]]
	then
		echo -e "\e[1m\e[31mIS G1 DEAD???\e[0m"
	elif [[ $nb == 1 ]]
	then
		echo -e "\e[1m\e[32mG1 is OK\e[0m"
	else
		echo -e "\e[1m\e[31mG1 is FORKING in $nb chains\e[0m"
	fi

	# Sleep for a while but still show something is happening and the script did not crash
	for (( i=$sleep ; i>0 ; i-- ))
	do
		sleep 1s
		printf "\r%02d:%02d" $((i/60)) $((i%60))
	done
	echo ""
done


